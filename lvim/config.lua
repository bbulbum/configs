-- general
vim.opt.timeoutlen = 100
vim.g.moonflyCursorColor = true
vim.g.nightflyUnderlineMatchParen = true
vim.g.moonflyWinSeparator = 2
vim.opt.fillchars = {
  horiz = '━', horizup = '┻', horizdown = '┳',
  vert = '┃', vertleft = '┫', vertright = '┣', verthoriz = '╋',
}

lvim.log.level = "warn"
lvim.format_on_save = true
lvim.colorscheme = "nightfly"
lvim.transparent_window = true
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false
lvim.builtin.cmp.formatting.duplicates.nvim_lsp = 1
lvim.builtin.cmp.confirm_opts.select = true

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.visual_mode["<leader>("] = "c()<esc>P"
lvim.keys.visual_mode["<leader>["] = "c[]<esc>P"
lvim.keys.visual_mode["<leader>{"] = "c{}<esc>P"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"

-- treesitter
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "tsx",
  "css",
  "rust",
  "java",
  "yaml",
}
lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true
lvim.builtin.treesitter.autotag.enable = true
lvim.builtin.treesitter.matchup.enable = true

-- LSP
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "tsserver" })

local function typescript_organize_imports()
  local params = {
    command = "_typescript.organizeImports",
    arguments = { vim.api.nvim_buf_get_name(0) },
    title = ""
  }
  vim.lsp.buf.execute_command(params)
end

local function typescript_rename_file()
  local source = vim.api.nvim_buf_get_name(0)
  vim.ui.input(
    { prompt = "New path: ", default = source },
    function(input)
      if input == "" or input == source or input == nil then
        return
      end

      local params = {
        command = "_typescript.applyRenameFile",
        arguments = { {
          sourceUri = vim.uri_from_fname(source),
          targetUri = vim.uri_from_fname(input),
        } }
      }
      vim.lsp.buf.execute_command(params)
      vim.loop.fs_rename(source, input)
      vim.cmd("e " .. input)
      vim.cmd("bd! " .. source)
    end
  )
end

require('lvim.lsp.manager').setup("tsserver", {
  root_dir = require('lspconfig').util.root_pattern(".git"),
  commands = {
    TypescriptOrganizeImports = {
      typescript_organize_imports,
      description = "Typescript Organize Imports"
    },
    TypescriptRenameFile = {
      typescript_rename_file,
      description = "Typescript Rename File"
    }
  },
  on_attach = function()
    vim.keymap.set('n', '<leader>o', ":TypescriptOrganizeImports<cr>", {})
    vim.keymap.set('n', '<leader>lR', ":TypescriptRenameFile<cr>", {})
  end
})

require('lvim.lsp.manager').setup("tailwindcss", {
  settings = {
    tailwindCSS = {
      experimental = {
        classRegex = { { "Class\\s*=\\s*{([^}]+)}", "\"([^\"]*)\"" } }
      }
    }
  }
})

require('lvim.lsp.manager').setup("pylsp")

-- format
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  {
    command = "prettierd",
    filetypes = { "typescript", "typescriptreact" },
  },
}
lvim.plugins = {
  {
    "windwp/nvim-ts-autotag",
    event = "InsertEnter",
  },
  {
    "mg979/vim-visual-multi"
  },
  {
    "edgedb/edgedb-vim"
  },
  {
    "bluz71/vim-nightfly-colors"
  }
}
