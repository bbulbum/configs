local function typescript_organize_imports()
  local params = {
    command = "_typescript.organizeImports",
    arguments = { vim.api.nvim_buf_get_name(0) },
    title = ""
  }
  vim.lsp.buf.execute_command(params)
end

local function typescript_rename_file()
  local source = vim.api.nvim_buf_get_name(0)
  vim.ui.input(
    { prompt = "New path: ", default = source },
    function(input)
      if input == "" or input == source or input == nil then
        return
      end

      local params = {
        command = "_typescript.applyRenameFile",
        arguments = { {
          sourceUri = vim.uri_from_fname(source),
          targetUri = vim.uri_from_fname(input),
        } }
      }
      vim.lsp.buf.execute_command(params)
      vim.loop.fs_rename(source, input)
      vim.cmd("e " .. input)
      vim.cmd("bd! " .. source)
    end
  )
end

local M = {}
M.setup = function()
  vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "tsserver" })

  require('lvim.lsp.manager').setup("tsserver", {
    commands = {
      TypescriptOrganizeImports = {
        typescript_organize_imports,
        description = "Typescript Organize Imports"
      },
      TypescriptRenameFile = {
        typescript_rename_file,
        description = "Typescript Rename File"
      }
    },
    on_attach = function()
      vim.keymap.set('n', '<leader>o', ":TypescriptOrganizeImports<cr>", {})
      vim.keymap.set('n', '<leader>lR', ":TypescriptRenameFile<cr>", {})
    end
  })
end

return M
