-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny

vim.opt.shiftwidth = 2        -- the number of spaces inserted for each indentation
vim.opt.tabstop = 2           -- insert 2 spaces for a tab
vim.opt.relativenumber = true -- relative line numbers
vim.opt.wrap = true           -- wrap lines

vim.o.timeoutlen = 100

lvim.colorscheme = "tokyonight-moon"
lvim.transparent_window = true
lvim.format_on_save.enabled = true

lvim.builtin.cmp.formatting.duplicates.nvim_lsp = 1
lvim.builtin.cmp.confirm_opts.select = true

lvim.keys.visual_mode["<leader>("] = "c()<esc>P"
lvim.keys.visual_mode["<leader>["] = "c[]<esc>P"
lvim.keys.visual_mode["<leader>{"] = "c{}<esc>P"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"

lvim.builtin.treesitter.autotag.enable = true
lvim.builtin.treesitter.matchup.enable = true

lvim.plugins = {
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  },
}
